package com.example.anggarisky.splashtohomeangga;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AutoActivity extends AppCompatActivity {


    private Button btnm;
    String server_url = "http://3.15.199.56/test.php";

    // URL to get contacts JSON


    ArrayList<HashMap<String, String>> contactList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto);
        btnm = (Button) findViewById(R.id.btnm);
        WebView web = (WebView) findViewById(R.id.web_view);
        web.loadUrl("http://3.15.199.56/status.php");
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        btnm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(AutoActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Intent explicit = new Intent(AutoActivity.this, ManualActivity.class);
                                startActivity(explicit);
                                Toast.makeText(AutoActivity.this, "Beralih Ke Mode Manual", Toast.LENGTH_LONG).show();
                                requestQueue.stop();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(AutoActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        requestQueue.stop();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("data", "2");
                        return params;
                    }

                };
                requestQueue.add(stringRequest);
            }


        });

    }

    public void onBackPressed() {
        Intent iLogin = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(iLogin);
    }

}