package com.example.anggarisky.splashtohomeangga;

import android.content.Intent;
import android.net.http.RequestQueue;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
//import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import java.util.HashMap;
import java.util.Map;

public class ManualActivity extends AppCompatActivity {
    Button button,skllampu,sklkipas,skltv;
    String server_url = "http://3.15.199.56/test.php";
    String url = "http://3.15.199.56/query.php";
    String surl = "http://3.15.199.56/ustatus.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual);
        button = (Button) findViewById(R.id.b_id_ok);
        skllampu = (Button) findViewById(R.id.lampu);
        sklkipas = (Button) findViewById(R.id.kipas);
        skltv = (Button) findViewById(R.id.tv);
        WebView web = (WebView) findViewById(R.id.web_view);
        web.loadUrl("http://3.15.199.56/status.php");
        web.setWebViewClient(new WebViewClient());
        web.getSettings().setJavaScriptEnabled(true);
        web.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        // TODO Auto-generated method stub
        final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(ManualActivity.this);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        int stat = Integer.parseInt(response);

                        if(stat == 111) {
                            skllampu.setText("Matikan");
                            sklkipas.setText("Matikan");
                            skltv.setText("Matikan");
                        }else if(stat == 110){
                            skllampu.setText("Matikan");
                            sklkipas.setText("Matikan");
                        }else if(stat == 100){
                            skllampu.setText("Matikan");
                        }else if(stat == 101){
                            skllampu.setText("Matikan");
                            skltv.setText("Matikan");
                        }else if(stat == 010){
                            sklkipas.setText("Matikan");
                        }else if(stat == 001){
                            skltv.setText("Matikan");
                        }else if(stat == 000){
                            //Toast.makeText(ManualActivity.this, "okeee", Toast.LENGTH_LONG).show();
                        }else{
                            sklkipas.setText("Matikan");
                            skltv.setText("Matikan");
                        }
                        requestQueue.stop();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ManualActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                error.printStackTrace();
                requestQueue.stop();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params = new HashMap<String, String>();
                params.put("data", "1");
                return params;
            }

        };
        requestQueue.add(stringRequest);



        skllampu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(ManualActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, surl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(ManualActivity.this, response, Toast.LENGTH_LONG).show();
                                if(skllampu.getText() == "Matikan") {
                                    skllampu.setText("Hidupkan");
                                }else{
                                    skllampu.setText("Matikan");
                                }
                                requestQueue.stop();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManualActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        requestQueue.stop();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("data", "1");
                        return params;
                    }

                };
                requestQueue.add(stringRequest);
            }


        });


        sklkipas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(ManualActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, surl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(ManualActivity.this, response, Toast.LENGTH_LONG).show();
                                if(sklkipas.getText() == "Matikan") {
                                    sklkipas.setText("Hidupkan");

                                }else{
                                    sklkipas.setText("Matikan");
                                }
                                requestQueue.stop();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManualActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        requestQueue.stop();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("data", "2");
                        return params;
                    }

                };
                requestQueue.add(stringRequest);
            }


        });

        skltv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(ManualActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, surl,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(ManualActivity.this, response, Toast.LENGTH_LONG).show();
                                if(skltv.getText() == "Matikan") {
                                    skltv.setText("Hidupkan");

                                }else{
                                    skltv.setText("Matikan");
                                }
                                requestQueue.stop();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManualActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        requestQueue.stop();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("data", "3");
                        return params;
                    }

                };
                requestQueue.add(stringRequest);
            }


        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                final com.android.volley.RequestQueue requestQueue = Volley.newRequestQueue(ManualActivity.this);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, server_url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Intent explicit = new Intent(ManualActivity.this, AutoActivity.class);
                                startActivity(explicit);
                                Toast.makeText(ManualActivity.this, "Beralih Ke Mode Otomatis", Toast.LENGTH_LONG).show();
                                requestQueue.stop();

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ManualActivity.this, "Error ...", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        requestQueue.stop();
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError{
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("data", "1");
                        return params;
                    }

                };
                requestQueue.add(stringRequest);
            }


        });


    }

    public void onBackPressed() {
        Intent iLogin = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(iLogin);
    }


}